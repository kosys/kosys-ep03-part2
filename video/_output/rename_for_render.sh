# レンダリング用ファイル名変更スクリプト
# レンダリング($ make)実行前にこのスクリプトを実行する

#ED.avi
#ED_subbed.avi
#EndCard.avi
#ETC_Continue.avi
#ETC_SupporterED.avi
#ETC_SupporterOP.avi
#NEXT_C01.avi
#NEXT_C02.avi
#OP.avi
#OP_C01.avi
#OP_C02.avi
#OP_C03.avi
#OP_C03a.avi
#OP_C04.avi
#OP_C05.avi
#OP_C06.avi
#OP_C07.avi
#OP_C08.avi
#OP_C09.avi
#OP_C10.avi
#OP_C11.avi
#OP_C12a.avi
#OP_C13.avi
#OP_C14.avi
#OP_C16.avi
#OP_C17.avi
#OP_C18.avi
#S03_C01.avi
#S03_C02.avi
#S03_C03.avi
#S03_C04.avi
#S03_C05.avi
#S03_C06.avi
#S03_C07.avi
#S03_C08.avi
#S03_C10b.avi
#S03_C12.avi
#S03_C13.avi
#S03_C14.avi
#S03_C15.avi
#S03_C17.avi
#S03_C18.avi
#S03_C19.avi
#S03_C20.avi
#S03_C21.avi
#S03_C23.avi
#S03_C25.avi
#S03_C26.avi
#S03_C27.avi
#S03_C28.avi
mv S03_C09.avi S03C09.avi
mv S03_C10a.avi S03C10a.avi
mv S03_C11.avi S03C11.avi
mv S03_C16.avi S03C16.avi
mv S03_C22.avi S03C22.avi
mv S03_C24.avi S03C24.avi
#S04_C01b.avi
#S04_C02.avi
#S04_C06.avi
#S04_C07.avi
#S04_C08.avi
#S04_C10.avi
#S04_C11.avi
#S04_C12.avi
#S04_C13.avi
#S04_C14.avi
#S04_C15.avi
#S04_C16.avi
#S04_C17.avi
#S04_C19.avi
#S04_C20.avi
#S04_C21.avi
#S04_C22.avi
#S04_C23.avi
#S04_C24.avi
#S04_C25.avi
mv S04_C01a.avi S04C01a.avi
mv S04_C03.avi S04C03.avi
mv S04_C04.avi S04C04.avi
mv S04_C05.avi S04C05.avi
mv S04_C09.avi S04-C09.avi
mv S04_C18.avi S04C18.avi
#S05_C03.avi
#S05_C05.avi
#S05_C07.avi
#S05_C10.avi
mv S05_C01.avi S05C01.avi
mv S05_C02.avi S05C02.avi
mv S05_C04.avi S05C04.avi
mv S05_C05.avi S05C05.avi
mv S05_C06.avi S05C06.avi
mv S05_C07.avi S05C07.avi
mv S05_C08.avi S05C08.avi
mv S05_C10.avi S05C10.avi
mv S05_C11.avi S05C11.avi
mv S05_C12.avi S05C12.avi
mv S05_C13.avi S05C13.avi
mv S05_C14.avi S05C14.avi
mv S05_C15.avi S05C15.avi
#S06_C01.avi
#S06_C02.avi
#S06_C04.avi
#S06_C06.avi
#S06_C07.avi
#S06_C08.avi
#S06_C14.avi
#S06_C16.avi
mv S06_C03.avi S06C03.avi
mv S06_C05.avi S06C05.avi
mv S06_C09.avi S06C09.avi
mv S06_C10.avi S06C10.avi
mv S06_C11.avi S06C11.avi
mv S06_C12.avi S06C12.avi
mv S06_C13.avi S06C13.avi
mv S06_C15.avi S06C15.avi
#TITLE_C01.avi
